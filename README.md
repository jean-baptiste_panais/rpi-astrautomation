This project is an astrophotography Python-based tool custom-designed for my own usage, however feel free to use it for you or to request anything !

[[_TOC_]]

# Equipment

- Canon 77D
- Raspberry Pi 3 B+
- USB - micro-USB Cable

# Rasperry Pi setup

Official documentation : https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi

## Download the OS

Go to https://www.raspberrypi.com/software/ and download the Raspberry Pi Imager software.

Once the software is installed, connect the SD card to your computer.

Good practice is to start by formatting the SD Card to make sure everything is good.

Then, you can install the OS. Since this project is designed to be a lightweight solution, you can select Raspberry PI OS Lite, which is what I used for the following steps.

## Enable SSH

To do this, you just need to create a file called "ssh" one the SD card where you installed the OS.

Open a terminal and go to the repository of the SD card, and then enter the following command :

```
touch ssh
```

## Enable WiFi

First we need, just as previously, to create a new file called "wpa_supplicant.conf"

```
touch wpa_supplicant.conf
```

Then, using the text editor of your choice, complete the following : 

```
country= #Your 2-digit country code (for example : FR)
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
network={
    ssid= #"YOUR_NETWORK_NAME" (for example : "Livebox-1234")
    psk= #"YOUR_PASSWORD"
    key_mgmt=WPA-PSK
}
```

## Packages installation

## Raspberry Pi as hotspot
Official documentation : https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-routed-wireless-access-point

# Software installation

## Service setup

This part's main goal is to run the software once the Raspberry Pi is launched. That way, all you need to do is to plug the Raspberry Pi in, and then with your smartphone or computer, connect to the Raspberry Pi's WiFi network and go to HTTP://192.168.4.1:5000/

```
touch /lib/systemd/system/cameraserver.service
sudo vim /lib/systemd/system/cameraserver.service
```

Add the following :

```
[Unit]
Description=My Camera Server Service
After=network.target

[Service]
User=pi
WorkingDirectory=/home/pi/
Restart=always
ExecStart=/usr/bin/python3 /home/pi/cameraserver.py

[Install]
WantedBy=multi-user.target
```

Then : 

```
sudo systemctl dameon-reload
sudo systemctl start cameraserver
```

# User manual

 TBW

# Future updates

- Fix exposure inconsistencies and precision ;
- Camera battery level ;
- Autoguiding.
