#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:46:18 2021

@author: jean-baptiste
"""

#import cv2
#import threading
import os
import sys
import time
from flask import Flask, render_template, session, request
import camera
#from PIL import Image

import gphoto2 as gp
import zwoasi as asi

#For RPi3 :
#asi.init("./ASI_linux_mac_SDK_V1.16.3/lib/armv8/libASICamera2.so")

#For PC :
#asi.init("/home/jean-baptiste/Téléchargements/ASI_linux_mac_SDK_V1.16.3/lib/x64/libASICamera2.so")

app = Flask(__name__)
app.secret_key = b'secret'
app._static_folder = os.path.abspath("/static/")

@app.route('/', methods=['GET', 'POST'])
def UI():
	if request.method == 'POST':
		session['texpo'] = request.form['texpo']
		session['nframes'] = request.form['nframes']
		
		try:
			dslr_camera = camera.get_dslr_camera()
		except:
			return "No camera found"
		camera.setup_dslr(dslr_camera)
		for i in range(int(session['nframes'])):
	        	print("Exposure "+str(i+1)+"/"+str(session['nframes']))
		        camera.bulb_image(dslr_camera,int(session['texpo']))
		        time.sleep(5)
	return render_template('layouts/main.html')

@app.route('/shutdown')
def shutdown():
    os.system("sudo shutdown -P +1")
    return render_template('layouts/shutdown.html')

@app.route('/settings')
def settings():
    os.system("sudo shutdown -P +1")
    return render_template('layouts/settings.html')

@app.route('/static/js/script.js')
def js_script():
    file = open('/home/pi/static/js/script.js','r')
    return file.read()

@app.route('/static/js/all.js')
def js_all():
    file = open('/home/pi/static/js/all.js','r')
    return file.read()

@app.route('/static/js/jquery-3.6.0.js')
def js_jquery():
    file = open('/home/pi/static/js/jquery-3.6.0.js','r')
    return file.read()

@app.route('/static/css/topbar.css')
def css_topbar():
    file = open('/home/pi/static/css/topbar.css', 'r')
    return file.read()

if __name__ == "__main__":
    os.system("clear")
    app.run(host="0.0.0.0")
