$(document).ready(function() {
  $('.toggle').click(function() {
    $('.nav-item').toggleClass('slide-out');
    $('.hamburger-1').toggleClass('cross-right');
    $('.hamburger-2').toggleClass('cross-hide');
    $('.hamburger-3').toggleClass('cross-left');
  });
  $('#settings').click(function() {
    document.location.href='/settings'
  });
  $('#shutdown').click(function() {
    document.location.href='/shutdown'
  });
});
